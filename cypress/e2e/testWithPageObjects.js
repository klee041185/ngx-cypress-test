///<reference types="cypress" />
import {onLoginPage} from "../support/PageObjects/loginPage"


describe('Test with Page Objects', () => {

    beforeEach('open application',() => {
        cy.visit('https://courses.ultimateqa.com/users/sign_in')
        cy.get('h2:contains("Welcome Back!")')
    })

    it('Verify Login Page - Empty Fields', () => {
      onLoginPage.enterCredentials(" ", " ")

    })
    it('Verify Login Page - Incorrect Password', () => {
      onLoginPage.enterCredentials("kev_lee2002@hotmail.com", "22paignton" )

    })
    it('Verify Login Page - Incorrect Username', () => {
      onLoginPage.enterCredentials("kev_lee_2002@hotmail.co", "22Paigton")

    })
    it('Verify Login Page - Forgotten Password', () => {
      onLoginPage.clickForgottenPassword()

    })

    it('Verify Login Page - Login Succesful', () => {
      onLoginPage.login()

    })

})

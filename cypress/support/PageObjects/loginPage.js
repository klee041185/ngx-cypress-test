

export class LoginPage{
  enterCredentials(username, password){
      const userName = username;
      const passWord = password
      cy.get('input[name="user[email]"]')
        .type(userName)
      cy.get('input[name="user[password]"]')
        .type(passWord)
      cy.get('button[type="submit"]')
        .click()
      cy.get('li:contains("Invalid email or password.")')
      cy.get('input[name="user[password]"]')
        .should('have.value', '')
  }

  clickForgottenPassword(){
      cy.get('a:contains("Forgot Password?")')
        .click()
      cy.get('h2:contains("Forgot your Password?")')
  }

  login(){
      cy.get('input[name="user[email]"]')
        .type("kev_lee2002@hotmail.com")
        .should('have.value', "kev_lee2002@hotmail.com")
      cy.get('input[name="user[password]"]')
        .type("22Paignton")
      cy.get('button[type="submit"]')
        .click()
      cy.get('a:contains("My Dashboard")')
  }

}

export const onLoginPage = new LoginPage()

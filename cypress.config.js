const { defineConfig } = require('cypress')

module.exports = defineConfig({
  projectId: 'n8dfo8',
  viewportHeight: 1080,
  viewportWidth: 1920,
  e2e: {
    baseUrl: 'https://ultimateqa.com/simple-html-elements-for-automation/',
    specPattern: 'cypress/e2e/**/*.{js,jsx,ts,tsx}',
    excludeSpecPattern: ['**/1-getting-started/*', '**/2-advanced-examples/*']
  }
})
